/**
   @file      test2d.cc
   @author    Christian Holm Christensen <cholmcc@gmail.com>
   @date      18 Aug 2021
   @copyright (c) 2021 Christian Holm Christensen. 
   @license   LGPL-3
   @brief     Testing of 2D histogram
   @see       @ref test2d.cc
*/
#include <histogram.hh>
#include <cassert>

int main() {
  using namespace nd_histogram;

  axis<double> a1{-1,-.5,-.2,0,.2,.5,1};
  axis<double> a2{0, .2, .5, 1};

  // Calculate bin widths
  std::valarray<double> rdx(a1.size()*a2.size());
  size_t i = 0;
  auto dx = a1.dx();
  auto dy = a2.dx();
  for (auto wy : dy) 
    for (auto wx : dx) 
      rdx[i++] = wx*wy;

  // Calculate mid-points
  std::valarray<std::array<double,2>>  rx(a1.size()*a2.size());
  i = 0;
  auto ay = a2.x();
  auto ax = a1.x();
  for (auto y : ay) 
    for (auto x : ax) 
      rx[i++] = {x,y};


  // Create histogram 
  histogram<2,double> h(a1,a2);
  auto  hdx = h.dx();
  auto  hx  = h.x();

  // Check widths and mid-points
  for (size_t i = 0; i < hx.size(); i++) {
    assert(std::abs(hdx[i]    - rdx[i])    < 1e-8);
    assert(std::abs(hx[i][0]  - rx[i][0])  < 1e-8);
    assert(std::abs(hx[i][1]  - rx[i][1])  < 1e-8);
  }

  // Check indexing 
  for (auto y : a2.bounds()) {
    int    iy = a2.find_bin(y);
    for (auto x : a1.bounds()) {
      int    ix = a1.find_bin(x);
      int    k  = h.find_bin(x,y);
      int    l  = h.index(ix,iy);
      assert(l == k);
    }
  }

  return 0;
}
// Local Variables:
//  compile-command: "g++ -g -I. test2d.cc -o test2d"
// End:
