/**
   @file      example1di.cc
   @author    Christian Holm Christensen <cholmcc@gmail.com>
   @date      18 Aug 2021
   @copyright (c) 2021 Christian Holm Christensen. 
   @license   LGPL-3
   @brief     Example of a 2D histogram 
   @see       @ref example2d.cc
*/
#include <histogram.hh>
#include <random>


//--------------------------------------------------------------------
int main() {
  using namespace nd_histogram;

  std::random_device                    rnd;
  std::default_random_engine            eng(rnd());
  std::normal_distribution<>            xdist;
  std::uniform_int_distribution<>       wdist(1,3);

  std::valarray<std::pair<double,int>> obs(1000);
  std::generate(std::begin(obs), std::end(obs),
		[&eng,&xdist,&wdist]() {
		  return std::make_pair(xdist(eng),wdist(eng)); });
  
  axis<double> ax(6,-3,3);  
  histogram<1,int,double> h(ax);
  
  for (auto o : obs) h.wfill(o.second,o.first);

  auto f = h.freeze();
  auto p = make_plotter(f);
  p.plot(std::cout, f, 50);
    
  return 0;
}

// Local Variables:
//  compile-command: "g++ -g -I. example1d.cc -o example1d"
// End:
