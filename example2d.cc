/**
   @file      example2d.cc
   @author    Christian Holm Christensen <cholmcc@gmail.com>
   @date      18 Aug 2021
   @copyright (c) 2021 Christian Holm Christensen. 
   @license   LGPL-3
   @brief     Example of a 2D histogram 
   @see       @ref example2d.cc
*/
#include <sstream>
#include <histogram.hh>
#include <random>
#include <cassert>

//====================================================================
void show_index_mapping(const nd_histogram::histogram<2,double>& h,
			std::ostream& o=std::cout) {
  
  o << "Index mapping - 2D case" << std::endl;

  const nd_histogram::axis<double>& ax = h.axes()[0];
  const nd_histogram::axis<double>& ay = h.axes()[1];

  size_t iw = 3;

  o << std::setw(iw) << " " << ": ";
  for (size_t i = 0; i < ax.size(); i++)
    o << std::setw(iw) << i << ": ";
  o << std::endl;
  
  for (size_t j = 0; j < ay.size(); j++) {
    o << std::setw(iw) << j << ": ";
    for (size_t i = 0; i < ax.size(); i++) {
      size_t idx = h.index(i,j);
      o << std::setw(iw) << idx << "  ";
    }
    o << std::endl;
  }
}

//--------------------------------------------------------------------
void show_widths(const nd_histogram::histogram<2,double>& h,
			std::ostream& o=std::cout) {
  
  o << "Bin widths - 2D case" << std::endl;

  const nd_histogram::axis<double>& ax = h.axes()[0];
  const nd_histogram::axis<double>& ay = h.axes()[1];

  constexpr size_t iw = 3;
  constexpr size_t vw = 6;
  constexpr size_t vp = 2;
  
  o << std::setw(iw) << " " << ": ";
  for (size_t i = 0; i < ax.size(); i++)
    o << std::setw(vw) << i << ": ";
  o << std::endl;
  
  for (size_t j = 0; j < ay.size(); j++) {
    o << std::setw(iw) << j << ": ";
    for (size_t i = 0; i < ax.size(); i++) {
      size_t idx = h.index(i,j);
      o << nd_histogram::wdouble<vw,vp>(h.dx()[idx]) << "  ";
    }
    o << std::endl;
  }
}

//--------------------------------------------------------------------
void show_coords(const nd_histogram::histogram<2,double>& h,
			std::ostream& o=std::cout) {
  
  o << "Mid-points - 2D case" << std::endl;

  const nd_histogram::axis<double>& ax = h.axes()[0];
  const nd_histogram::axis<double>& ay = h.axes()[1];

  constexpr size_t iw = 3;
  constexpr size_t vw = 5;
  constexpr size_t vp = 2;
  
  o << std::setw(iw) << " " << ": ";
  for (size_t i = 0; i < ax.size(); i++)
    o << std::setw(2*vw+1) << i << ": ";
  o << std::endl;
  
  for (size_t j = 0; j < ay.size(); j++) {
    o << std::setw(iw) << j << ": ";
    for (size_t i = 0; i < ax.size(); i++) {
      size_t idx = h.index(i,j);
      o << nd_histogram::wdouble<vw,vp>(h.x()[idx][0]) << ","
	<< nd_histogram::wdouble<vw,vp>(h.x()[idx][1]) << "  ";
    }
    o << std::endl;
  }
}

//--------------------------------------------------------------------
void show_dwdx(const nd_histogram::frozen_histogram<2,double>& h,
	       std::ostream& o=std::cout) {
  
  o << "dw/dx - 2D case" << std::endl;

  constexpr size_t iw = 3;
  constexpr size_t vw = 8;
  constexpr size_t vp = 2;
  
  o << std::setw(iw) << " " << ": ";
  for (size_t i = 0; i < h.n[0]; i++)
    o << std::setw(vw) << i << ": ";
  o << std::endl;

  for (size_t j = 0; j < h.n[1]; j++) {
    o << std::setw(iw) << j << ": ";
    for (size_t i = 0; i < h.n[0]; i++) {
      size_t idx = h.index(i,j);
      o << nd_histogram::wdouble<vw,vp>(h.dwdx[idx]) << "  ";
    }
    o << std::endl;
  }
}

//--------------------------------------------------------------------
void show_edwdx(const nd_histogram::frozen_histogram<2,double>& h,
		std::ostream& o=std::cout) {
  
  o << "Uncertainty on dw/dx - 2D case" << std::endl;

  constexpr size_t iw = 3;
  constexpr size_t vw = 8;
  constexpr size_t vp = 3;
  
  o << std::setw(iw) << " " << ": ";
  for (size_t i = 0; i < h.n[0]; i++)
    o << std::setw(vw) << i << ": ";
  o << std::endl;

  for (size_t j = 0; j < h.n[1]; j++) {
    o << std::setw(iw) << j << ": ";
    for (size_t i = 0; i < h.n[0]; i++) {
      size_t idx = h.index(i,j);
      o << nd_histogram::wdouble<vw,vp>(h.edwdx[idx]) << "  ";
    }
    o << std::endl;
  }
}

//--------------------------------------------------------------------
void rw(const nd_histogram::histogram<2,double>& h) {
  std::stringstream stream;

  nd_histogram::formatted_io::write_histogram(stream, h);
  std::cout << "Streamed histogram:\n  " << stream.str() << std::endl;
  // auto ax = nd_histogram::io::read_axis<double>(stream);
  // auto ay = nd_histogram::io::read_axis<double>(stream);
  // auto h2 = nd_histogram::io::read_histogram<2,double>(stream,ax,ay);
  auto h2 = nd_histogram::formatted_io::read_histogram<2,double>(stream);
  assert(h == h2);
}


//--------------------------------------------------------------------
int main() {
  using namespace nd_histogram;

  axis<double> ax(6,-3,3);
  axis<double> ay{0.1, .2, .5, 1, 3};
  
  histogram<2,double> h(ax,ay);

  show_index_mapping(h);
  show_widths(h);
  show_coords(h);
  
  std::random_device              rnd;
  std::default_random_engine      eng(rnd());
  std::normal_distribution<>      xdist;;
  std::exponential_distribution<> ydist;

  std::valarray<std::pair<double,double>> obs(100000);
  std::generate(std::begin(obs), std::end(obs),
		[&eng,&xdist,&ydist]() {
		  return std::make_pair(xdist(eng),ydist(eng)); });
  
  for (auto o : obs)
    h.fill(o.first, o.second);

  rw(h);

  auto f = h.freeze();
  
  show_dwdx (f);
  show_edwdx(f);

  
  return 0;
}

// Local Variables:
//  compile-command: "g++ -g -I. example2d.cc -o example2d"
// End:
