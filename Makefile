#
#
#
CXX		:= g++
CPPFLAGS	:= -I.
CXXFLAGS	:=
LD		:= g++
LDFLAGS		:=
LIBS		:=

HEADERS		:= histogram.hh		
SOURCES		:= example1d.cc		\
		   example1di.cc	\
		   example2d.cc		\
		   test2d.cc		\
		   test3d.cc		\
		   testadd.cc		\
		   testio.cc
PROGRAMS	:= $(SOURCES:%.cc=%)
DOCS		:= doc/Doxyfile 	\
		   doc/DoxygenLayout.xml

%:%.cc
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) $< $(LDFLAGS) $(LIBS) -o $@

all:	$(PROGRAMS)

clean:
	rm -f $(PROGRAMS)
	rm -f *~ *.o *.gch *.dat
	rm -rf html latex

doc:	$(DOCS) $(HEADERS) $(SOURCES)
	doxygen $<

test:	$(PROGRAMS)
	@$(foreach p, $^, \
	   (printf "Test: %-20s " "$(p)" ; ./$(p) > /dev/null 2>&1 && \
	      echo "passed" || (echo "failed"; false)) && ) true

example1d:	example1d.cc	histogram.hh
example1di:	example1di.cc	histogram.hh
example2d:	example2d.cc	histogram.hh
test2d:		test2d.cc	histogram.hh 	
test3d:		test3d.cc	histogram.hh 	
testadd:	testadd.cc	histogram.hh 	
testio:		testio.cc	histogram.hh 	

#
# EOF
#

