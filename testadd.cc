/**
   @file      testadd.cc
   @author    Christian Holm Christensen <cholmcc@gmail.com>
   @date      18 Aug 2021
   @copyright (c) 2021 Christian Holm Christensen. 
   @license   LGPL-3
   @brief     Testing of 2D histogram
   @see       @ref testadd.cc
*/
#include <histogram.hh>
#include <cassert>
#include <random>
#include <iterator>

int main() {
  using namespace nd_histogram;

  axis<double> a1{-1,-.5,-.2,0,.2,.5,1};
  axis<double> a2{0, .2, .5, 1};

  // Create histograms 
  histogram<2,double> h1(a1,a2);
  histogram<2,double> h2(a1,a2);

  std::random_device              rnd;
  std::default_random_engine      eng(rnd());
  std::normal_distribution<>      xdist;;
  std::exponential_distribution<> ydist;

  std::valarray<std::pair<double,double>> obs(100000);
  std::generate(std::begin(obs), std::end(obs),
		[&eng,&xdist,&ydist]() {
		  return std::make_pair(xdist(eng),ydist(eng)); });
  
  for (auto o : obs) {
    h1.fill(o.first, o.second);
    h2.fill(o.first, o.second);
  }

  // Add second histogram to first
  h1       += h2;
  auto w2  =  h2.w();
  auto w1  =  h1.w();
  auto d21 =  std::abs(2*w2 - w1);

  assert(d21.max() < 1e-9);
  
  auto h3  = h2 + h2;
  assert(h3 == h1); 

  return 0;
}
// Local Variables:
//  compile-command: "g++ -g -I. testadd.cc -o testadd"
// End:
