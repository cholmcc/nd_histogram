/**
   @file      testio.cc
   @author    Christian Holm Christensen <cholmcc@gmail.com>
   @date      18 Aug 2021
   @copyright (c) 2021 Christian Holm Christensen. 
   @license   LGPL-3
   @brief     Testing of I/O of histogram
   @see       @ref testio.cc
*/
#include "histogram.hh"
#include <random>
#include <fstream>
#include <cassert>

template <typename IO>
nd_histogram::histogram<1,double>
test_io(const nd_histogram::histogram<1,double>& hist,
	const std::string& ext)
{
  std::ofstream fout(("test."+ext).c_str());
  fout.fill(IO::template trait_type<double>::sep);
  IO::template write_histogram<1,double,double>(fout,hist);
  fout.close();

  std::ifstream fin(("test."+ext).c_str());
  fin.fill(IO::template trait_type<double>::sep);
  auto raxis = IO::template read_axis<double>(fin);
  auto rhist = IO::template read_histogram<1,double,double>(fin,raxis);
  fin.close();

  return rhist;
}

  

int main()
{
  std::random_device                    rnd;
  std::default_random_engine            eng(12345 /*rnd()*/);
  std::normal_distribution<>            xdist;;

  nd_histogram::axis<double>        axis(15,-3,3);
  nd_histogram::histogram<1,double> hist(axis);

  std::valarray<double> obs(1000);
  std::generate(std::begin(obs), std::end(obs),
		[&eng,&xdist]() { return xdist(eng); });

  for (auto o : obs) hist.fill(o);

  nd_histogram::ascii_plotter<1,double,double>::plot(std::cout, hist);

  std::cout << "Read back histogram, ASCII file" << std::endl;
  auto nhist = test_io<nd_histogram::formatted_io>(hist,"nd");

  nd_histogram::ascii_plotter<1,double,double>::plot(std::cout, nhist);
  assert(nhist == hist);

  std::cout << "Read back histogram, Binary file" << std::endl;
  auto bhist = test_io<nd_histogram::binary_io>(hist,"bnd");
  
  nd_histogram::ascii_plotter<1,double,double>::plot(std::cout, bhist);
  assert(bhist == hist);
  
  return 0;
}
