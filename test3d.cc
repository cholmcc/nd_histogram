/**
   @file      test3d.cc
   @author    Christian Holm Christensen <cholmcc@gmail.com>
   @date      18 Aug 2021
   @copyright (c) 2021 Christian Holm Christensen. 
   @license   LGPL-3
   @brief     Testing of 3D histogram
   @see       @ref test3d.cc
*/
#include <histogram.hh>
#include <cassert>

int main() {
  using namespace nd_histogram;

  axis<double> a1{-1,-.5,-.2,0,.2,.5,1};
  axis<double> a2{0, .2, .5, 1};
  axis<double> a3{0, .3, .7};

  // Calculate bin widths
  std::valarray<double> rdx(a1.size()*a2.size()*a3.size());
  size_t i = 0;
  auto dz = a3.dx();
  auto dy = a2.dx();
  auto dx = a1.dx();
  for (auto wz : dz) 
    for (auto wy : dy) 
      for (auto wx : dx) 
	rdx[i++] = wx*wy*wz;

  // Calculate mid-points
  std::valarray<std::array<double,3>>  rx(a1.size()*a2.size()*a3.size());
  i = 0;
  auto az = a3.x();
  auto ay = a2.x();
  auto ax = a1.x();
  for (auto z : az) 
    for (auto y : ay) 
      for (auto x : ax) 
	rx[i++] = {x,y,z};


  // Create histogram 
  histogram<3,double> h(a1,a2,a3);
  auto  hdx = h.dx();
  auto  hx  = h.x();

  // Check widths and mid-points
  for (size_t i = 0; i < hx.size(); i++) {
    assert(std::abs(hdx[i]    - rdx[i])    < 1e-8);
    assert(std::abs(hx[i][0]  - rx[i][0])  < 1e-8);
    assert(std::abs(hx[i][1]  - rx[i][1])  < 1e-8);
    assert(std::abs(hx[i][2]  - rx[i][2])  < 1e-8);
  }

  return 0;
}
// Local Variables:
//  compile-command: "g++ -g -I. test2d.cc -o test2d"
// End:
