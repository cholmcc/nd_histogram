# nd_histogram

A generic histogram in any number of dimensions

This provide histogramming in any number of dimension using really
simple classes with minimal overhead (except that sparse histograms
are not implemented).  The code is only some 500 lines of code,
including I/O code. 

## API walk-through 

Below is a short walk-through of the API.

Axes are declared using the class template `nd_histogram::axis` - for
example

~~~{.cc}
nd_histogram::axis xaxis(30,-3,3); // Equidistant bins 
nd_histogram::axis yaxis{0,.5,1,3,6,10}; // Variable bin size 
~~~
	
Axes objects can be shared by different `nd_histogram::histogram`
objects.  One then pass a suitable number of axes objects to the class
template instance constructor

~~~{.cc}
nd_histogram::histogram<double,1> h1(xaxis); // One-dimensional 
nd_histogram::histogram<double,2> h2(xaxis,yaxis); // Two-dimensional 
~~~

Histograms of _any_ dimensionality can be decalared (up to some
memory boundary).

We can then fill in (weighted) observations

~~~{.cc}
std::random_device                rnd;
std::default_random_engine        eng(rnd()); // Or explicit seed 
std::exponentional_distribution<> ydist;
std::normal_distribution<>        xdist;
std::uniform_distribution<>       wdist;

// Arrays of observations 
std::valarray<double>                   xobs(1000); 
std::valarray<std::pair<double,double>> xyobs(1000);

// Use generator to make random X observations 
std::generate(std::begin(xobs),std::end(yobs), 
              [&eng,&xobs](){ return xdist(eng); });

// Associate Y observations with X observations via transform 
std::transform(std::begin(xobs),std::end(xobs), std::begin(xyobs),
               [&eng,&ydist](auto x) { std::make_pair(x,ydist(eng)); });

// Fill in observations 
for (auto x : xobs) h1.fill(x);
for (auto xy : xyobs) h2.wfill(wdist(eng),xy.first,xy.second);
~~~

When we have accumulated statistics, we can query the histogram object.

To get the bin centres (possibly multi-dimensional), use
`nd_histogram::histogram::x`

~~~{.cc}
auto x1 = h1.x();
auto x2 = h2.x();
for (auto x : x1) std::cout << x << std::endl;
for (auto x : x2) std::cout << x[0] << "," << x[1] << std::endl;
~~~

To get the density _dw/dx_ (that is the sums of weights divided by the
bin widths), use `nd_histogram::histogram::dwdx`

~~~{.cc}
auto dwdx1 = h1.dwdx();
auto dwdx2 = h2.dwdx();
for (auto y : dwdx1) std::cout << y << std::endl;
for (auto y : dwdx2) std::cout << y << std::endl;
~~~

To get the uncertainty on _dw/dx_ use `nd_histogram::histogram::edwdx`

~~~{.cc}
auto edwdx1 = h1.edwdx();
auto edwdx2 = h2.edwdx();
for (auto e : dwdx1) std::cout << e << std::endl;
for (auto e : dwdx2) std::cout << e << std::endl;
~~~

Note that these member functions `nd_histogram::histogram::x`,
`nd_histogram::histogram::dwdx`, and `nd_histogram::histogram::edwdx`
(and `nd_histogram::histogram::dx` and `nd_histogram::histogram::w`)
returns flat arrays (`std::valarray`).  To index a specific bin, use
`nd_histogram::histogram::index`

~~~{.cc}
std::cout << dwdx1[h1.index(2)] << std::endl;
std::cout << dwdx2[h2.index(2,3)] << std::endl;
~~~

or `nd_histogram::histogram::find_bin` to map from axes values to
index.  Note also that these member functions return _by value_.  That
is, the returned arrays are the user's to manage.   This also means
that these member functions perform _calculations_ on every call.  One
should therefore consider to use `nd_histogram::frozen_histogram` in
case one needs to access the data of a histogram often.  That class
template will store the values of the histogram as constant values. 

## One-dimensional caveat

In the case of one-dimension, the member function
`nd_histogram::histogram::x` still returns

~~~{.cc}
std::valarray<T,std::array<T,1>> x = hist.x();  
~~~

which means we have to get the values by 

~~~{.cc} 
auto val = x[bin][0]
~~~

## Ordering of flat arrays 

The flat arrays returned by the member functions
`nd_histogram::histogram::x`, `nd_histogram::histogram::dwdx`, and
`nd_histogram::histogram::edwdx` (and `nd_histogram::histogram::dx`
and `nd_histogram::histogram::w`) are ordered according to normal
C-ordering.  That is, the outer most dimension increases first, then
the second, the third, and so on.

For example, for a three-dimension histogram 

- _x_ varies first, then 
- _y_, and finally 
- _z_.  

That means that the indexes (_i_,_j_,_k_) (corresponding to
(_x_,_y_,_z_) of sizes (_nx_,_ny_,_nz_)) maps to element

     ((k * ny + j) * nx) + i;

Thus, if one loops first of _z_, then _y_, and finally _x_, then one
can access the elements sequentially 

~~~{.cc}
auto x     = h.x()
auto iter  = std::begin(x);

for (size_t k = 0; k < nz; k++) { 
  for (size_t j = 0; j < ny; j++) { 
    for (size_t i = 0; i < nx; i++, ++iter) {
	  // Do something with x = *iter 
    }
  }
}
~~~

## Other operations 

Scaling of histograms _will not_ be supported.  A scaled histogram is
no longer a histogram. Instead, retrieve the data and work on that.

Multiplication, division, and subtraction of histograms _will not_ be
supported.  The product, ratio, or difference of a histogram with _any
thing_ is no longer a histogram.  Instead, retrieve the data and work
on that.

Addition of histograms _is_ supported.  This corresponds to adding
more observations to an existing histogram.


## Authors and acknowledgment

Copyright (c) 2021 Christian Holm Christensen <cholmcc@gmail.com>

## License

Lesser GNU General Public License version 3

