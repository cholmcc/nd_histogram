/**
   @file      histogram.hh
   @author    Christian Holm Christensen <cholmcc@gmail.com>
   @date      18 Aug 2021
   @copyright (c) 2021 Christian Holm Christensen. 
   @license   LGPL-3
   @brief     A versatile mutli-dimensional histogram 
   @see       @ref nd_histogram

   A generic histogram in any number of dimensions

   This provide histogramming in any number of dimension using really
   simple classes with minimal overhead (except that sparse histograms
   are not implemented).  The main part of the code is only some 250
   lines of code, with under 200 lines of supplementary code. 

   Scaling of histograms _will not_ be supported.  A scaled histogram
   is no longer a histogram. Instead, retrieve the data and work on that.

   Multiplication, division, and subtraction of histograms _will not_
   be supported.  The product, ratio, or difference of a histogram
   with _any thing_ is no longer a histogram.  Instead, retrieve the
   data and work on that.

   Addition of histograms _will_ be supported.  This corresponds to
   adding more observations to an existing histogram.

   This is available from https://gitlab.com/cholmcc/nd_histogram
*/
#ifndef ND_HISTOGRAM_HISTOGRAM
#define ND_HISTOGRAM_HISTOGRAM
#include <valarray>
#include <array>
#include <numeric>
#include <functional>
#include <stdexcept>
#include <iostream>
#include <iomanip>
#include <iterator>
#include <vector>
#include <variant>

/** 
   @defgroup nd_histogram Generic N-dimensional histogram classes
   
   @see @ref index
*/
#if 0
// In case we need to print valarray objects
namespace std
{
  template <typename T>
  ostream& operator<<(ostream& o, const std::valarray<T>& v) {
    stringstream s;
    copy(begin(v),end(v), ostream_iterator<T>(s, ","));
    string str = s.str();
    if (str.length() > 0) str.pop_back();
    return o << "[" << str << "]";
  }
}
#endif
      
/** 
   Namespace for histogramming code 

   @ingroup nd_histogram 
*/
namespace nd_histogram
{
  // Forward declaration
  template <size_t N, typename W, typename X>
  struct frozen_histogram;

  

  //==================================================================
  /** 
      
     Histogram axis 
      
     Instances of this template defines a class for a one-dimensional
     axis of bins.  These bins can be equidistant (of same width) or
     of variable width.  The instances of this class template defines
     member functions to

     - Look up a bin index (nd_histogram::axis::find_bin)

     - Get the widths (nd_histogram::axis::widths) and centers
       (nd_histogram::axis::centers) of all bins.

     - Get the number of bins (nd_histogram::axis::size).

     Objects of instances of the class template nd_histogram::axis
     can be shared among different uses - once defined the objects are
     immutable.

     @see @ref index

     @ingroup nd_histogram 
  */
  template <typename X=double>
  struct axis {
    /** Value type */
    using x_type=X;
    
    /** Value array */
    using x_array=std::valarray<x_type>;
    
    /** 
	Constructor.  This will create equidistant bins
	
	@param nbins Number of bins 
	@param low   Low edge 
	@param high  High edge 
    */
    axis(size_t nbins, x_type low, x_type high)
      : _bounds(nbins+1) {
      x_type  dx = (high - low) / nbins;
      
      std::iota(std::begin(_bounds),std::end(_bounds),x_type());
      
      _bounds *= dx;
      _bounds += low;
    }
    /** 
	Constructor.  Initialize from an initializer list 
	
	@param bounds bin boundaries 
    */
    axis(std::initializer_list<x_type> bounds) : _bounds(bounds) {}
    /** 
	Constructor.  Initialize from an initializer list 
	
	@param bounds bin boundaries 
    */
    axis(const x_array& bounds) : _bounds(bounds) {}
    /** Copy constructor */
    axis(const axis<X>& other) : _bounds(other._bounds) {}
    /** Move constructor */
    axis(axis<X>&& other) : _bounds(std::move(other._bounds)) {}
    /** No default contructor */
    axis() = delete;
    /** 
	Find bin index corresponding to @a x 
	
	@param x  Value to find bin index for 
	
	@return bin index or -1 if out of bounds 
    */
    int find_bin(const x_type x) const {
      if (x == _bounds[size()])
	return size()-1;
      
      auto s = std::begin(_bounds);
      auto e = std::end(_bounds);
      auto i = std::upper_bound(s,e,x);
      if (i == e) return -1;
      
      return std::distance(s,i)-1;
    }
    
    /** Bin widts */
    x_array dx() const {
      size_t n = size();
      return _bounds[std::slice(1,n,1)]-_bounds[std::slice(0,n,1)];
    }
    
    /** Bin centres */
    x_array x() const {
      size_t n = size();
      return (_bounds[std::slice(1,n,1)]+_bounds[std::slice(0,n,1)])/2;
    }
    
    /** Number of bins */
    size_t size() const { return _bounds.size() - 1; }

    /** Bin boundaries */
    const x_array& bounds() const { return _bounds; }

    /** 
	Is other axix compatible? 

	- Absolute tolerance is @f$ 10^{-8}@f$ 
	- Relative tolerance is @f$ 10^{-5}@f$

	@return true if compatible (same bin boundaries)
    */
    template <typename T1>
    bool operator==(const axis<T1>& other) const {
      return std::abs(_bounds - other._bounds).max()
	< (1e-8 + 1e-5 * std::abs(other._bounds).max());
    }
  protected:
    x_array _bounds;
  };

  //==================================================================
  /**
     @{
     @name Deduce density type based on weight and bin types
  */
  /** Default - same as weight */
  template <typename X, typename W, class Enable=void>
  struct density_type { using type=W; };
  /** For integral bin type, same as weight */
  template <typename X, typename W>
  struct density_type<X,W,
		      typename std::enable_if<std::is_integral<X>::value>::type>
  {
    using type=W;
  };
  /** For floating point bins, same as bins */
  template <typename X, typename W>	  
  struct density_type<X,W,
		      typename std::enable_if<std::is_floating_point<X>::value>::type>
  {
    using type=X;
  };
  
  //==================================================================
  /** 
      A histogram in any dimension. 

      Objects of instances of this class template has a fixed
      dimensionality (second template parameter) and a fixed data type
      (first template parameter).  

      @todo Axis type (x_type) and weight (w_types) types should be
      separete.  For example, the weight type could be integer, while
      the X type is real.  Challenge really only comes when we project
      to a frozen_histogram and when calculating densities.
      
      @see @ref index 

      @ingroup nd_histogram
  */
  template <size_t N, typename W, typename X=double>
  struct histogram
  {
    /** Type of sum of weights */
    using w_type = std::valarray<W>;
    /** Element type */
    using e_type = W;


    /** Axis type */
    using axis_type=axis<X>;
    /** Type of x values */
    using x_type = std::valarray<std::array<X,N>>;
    /** Type of dx values */
    using dx_type = std::valarray<typename axis_type::x_type>;
    /** Type of derived values */
    using d_type = typename density_type<typename axis_type::x_type,
					 e_type>::type;
    /** Type of dw values */
    using dw_type = std::valarray<d_type>;
    /** Type of container of references to axes */
    using axes_type = std::array<const axis_type,N>;
    /** Corresponding frozen histogram */
    using frozen = frozen_histogram<N,W,X>;
    /** Operation on the histogram */
    using freezer =
      std::tuple<w_type,w_type>(*)(const histogram<N,W,X>&);
    /** The number of dimensions */
    const size_t dimension;

    /** Construct from array of axis objects */
    histogram(const axes_type& axes)
      : dimension(N),
	_axes{axes},
	_w(_size(axes))
    {
    }
    /** 
	Constructor 

	Construct histogram from @f$ N@f$ axis objects 

	@param axes The axis objects 
    */
    template <typename ... Axes>
    histogram(const Axes& ... axes)
      : dimension(N),
	_axes{axes...},
	_w(_size(axes...))
    {
      static_assert(N == sizeof...(axes));
    }
    /** No default constructor */
    histogram() = delete;
    /** No default copy constructor */
    histogram(const histogram<N,W,X>&) = delete;
    /** Move constructor */
    histogram(histogram<N,W,X>&& other)
      : dimension(N),
     	_axes(std::move(other._axes)),
     	_w(std::move(other._w))
    {
    }
	      
			
			
    /** 
	@{ 
	@name Filling in observations 

	@return true if observation was within bounds
    */
    /** 
	Fill in an observersion 

	@param x Observation 
    */
    template <typename ... Xs>
    bool fill(const Xs& ... x) {
      static_assert(sizeof...(x) == N);

      int idx = _find_bin(0,x...);
      if (idx < 0) return false;

      _w[idx] += 1;

      return true;
    }
    /** 
	Fill in an observersion 

	@param w Weight
	@param x Observation 

	@return true if observation was within bounds
    */
    template <typename WW, typename ... Xs>
    bool wfill(const WW& w, const Xs& ... x) {
      static_assert(sizeof...(x) == N);

      int idx = _find_bin(0,x...);
      if (idx < 0) return false;

      _w[idx] += w;
      return true;
    }
    /** @} */
    
    /** 
	@{ 
	@name Indexing 
    */
    /** 
	Find bin index correspnding to values.  Returns -1 if out of bounds. 

	@param x Values 
	
	@returns Bin index or -1 if out of bounds 
    */
    template <typename ... Xs>
    int find_bin(const Xs& ... x) {
      static_assert(sizeof...(x) == N);
      
      return _find_bin(0,x...);
    }

    /** Get real index */
    template <typename ... Is>
    int index(const Is& ... i) const {
      static_assert(sizeof...(Is) == N);

      return _index(0,i...);
    }
    /** 
	Get array of axes references 
    */
    const axes_type& axes() const { return _axes; }
    /** @} */
    
    /** 
	@{ 
	@name Data 
    */
    /** 
	Get bin width @f$\Delta@f$ in each bin
    */
    dx_type dx() const {
      dx_type ret(1,_w.size());
      size_t stride = 1;
      for (auto ia = _axes.begin(); ia != _axes.end(); ++ia) {
	_aw(ret, stride, *ia);
	stride *= ia->size();
      }

      return ret;
    }

    /** 
	Get bin centre @f$ x@f$ in each bin 
    */
    x_type x() const {
      x_type ret(_w.size());
      size_t stride = 1;
      size_t na     = 0;
      for (auto ia = _axes.begin(); ia != _axes.end(); ++ia, ++na) {
	_ac(ret, na, stride, *ia);
	stride *= ia->size();
      }
      
      return ret;
    }

    /** 
	Get sum of weights 

	@f[\sum_i w_i\quad,@f]

	in each bin.
    */
    const w_type& w() const { return _w;  }

    /** 
	Get 

	@f[ \frac{\mathrm{d}w}{\mathrm{d}x} 
	= \frac{\sum_i w_i}{\Delta}\quad, 
	@f]

	in each bin. That is, the sum of weights divided by the bin widths.
    */
    dw_type dwdx() const { return _w_as_dw() / _dx_as_dw(); }
    /** 
	Get uncertainty on @f$ dw/dx@f$ - that is, the square root sum
	of weights divided by the bin widths.
    */
    dw_type edwdx() const { return std::sqrt(_w_as_dw()) / _dx_as_dw(); }
    /** @} */

    /**
       Get the integral
       @f[ \sum_i w_i \Delta_i @f]

    */
    d_type integral() const
    {
      return (_w * dx()).sum();
    }

    /** @{
	@name Create frozen histograms */
    /**
       A simple frozen histogram
    */
    frozen freeze() { return frozen_histogram(*this); }
    /**
       Freeze histogram and apply Freezer operation to the frozen value
    */
    template <typename Freezer>
    frozen freeze(Freezer oper) { return frozen_histogram(*this,oper); }
    /** @} */
    /** 
	@{ 
	@name Other histograms 
    */
    /** 
	Check if other histogram is compatible 

	@param h Other histogram to check for compatibility 

	@return true if other is compatible with this
    */
    template <typename W1,typename X1>
    bool is_compatible(const histogram<N,W1,X1>& h) const {
      auto oia = h._axes.begin();
      auto tia = _axes.begin();
      for (; tia != _axes.end(); ++oia, ++tia)
	if (not (*tia == *oia)) return false;
      return true;
    }
    /** 
	Add another histogram to this
	
	- Absolute tolerance is @f$ 10^{-8}@f$ 
	- Relative tolerance is @f$ 10^{-5}@f$
	
	@param other Right-hand side 

	@return Sum of the this and other histogram 

	@throws std::logic_error in case the histograms are not compatible 
    */
    template <typename W1,typename X1>
    histogram& operator+=(const histogram<N,W1,X1>& other) {
      if (not is_compatible(other))
	throw std::invalid_argument("+=: incompatible histograms");

      _w += other._w;
      return *this;
    }
    /** 
	Compare this to another histogram 
	
	@param other Right-hand side 
	
	@return true if the histograms are compatible and have same
	sum of weights 
    */
    template <typename W1,typename X1>
    bool operator==(const histogram<N,W1,X1>& other) const {
      if (not is_compatible(other)) return false;

      return std::abs(_w - other._w).max()
	< (1e-8 + 1e-5 * std::abs(other._w).max());
    }
    /** @} */
  protected:
    /** 
	@{ 
	@name Number of bins 
    */
    /** 
      Calculate total size (number of bins) of the histogram using a
      recursive template expansion (terminating template)

      @param axes  Axis objects 

      @returns Number of bins 
    */
    size_t _size(const axes_type& axes) const {
      size_t ret = 1;
      for (auto a : axes) ret *= a.size();
      return ret;
    }
    /** 
      Calculate total size (number of bins) of the histogram using a
      recursive template expansion.

      @param a  Axis object 
      @param as Remaining axes objects 

      @returns Number of bins 
    */
    template <typename A, typename ... As>
    size_t _size(const A& a, const As& ... as) const {
      return a.size() * _size(as...);
    }
     /** 
       Calculate total size (number of bins) of the histogram using a
       recursive template expansion (terminating template)
       
       @param a  Axis object 
       
       @returns Number of bins 
     */
    template <typename A>
    size_t _size(const A& a) const { return a.size(); }
    /** @} */

    /** 
	@{
	@name Bin widths 
    */
    /** 
	For a given axis, multiply widths by axis widths 
	
	@param ret    Return array 
	@param stride Stride to use 
	@param a      Axis 
    */
    void _aw(dx_type& ret, size_t stride, const axis_type& a) const {
      auto      dx      = a.dx();
      size_t    chunk   = stride * dx.size();
      size_t    repeat  = ret.size() / chunk;

      for (size_t i = 0; i < repeat; i++) {
	size_t off = i * chunk;
	for (size_t j = 0; j < stride; j++) {
	  ret[std::slice(off+j,dx.size(),stride)] *= dx;
	}
      }
    }
    /** @} */

    /** 
	@{
	@name Bin centres
    */
    /** 
	For a given axis, set X field 
	
	@param ret    Return array 
	@param ia     Axis index 
	@param stride Stride to use 
	@param a      Axis 
    */
    void _ac(x_type& ret, size_t ia, size_t stride, const axis_type& a) const {
      auto      x       = a.x();
      size_t    chunk   = stride * x.size();
      size_t    repeat  = ret.size() / chunk;

      for (size_t i = 0; i < repeat; i++) {
	size_t off = i * chunk;
	for (size_t j = 0; j < stride; j++) {
	  for (size_t k = 0; k < x.size(); k++) {
	    ret[off+j+stride*k][ia] = x[k];
	  }
	}
      }
    }
    /** @} */

    /** 
	@{ 
	@name Index from indexes 
    */
    /** 
	Get index correspnding to axes indexes. 

	@param ai   Axes number 
	@param i    First axis index 
	@param is   Remaming axes indexes 
	
	@return Global index 
    */
    template <typename I, typename ... Is>
    int _index(size_t ai, const I& i, const Is& ... is) const {
      if (i < 0 or i >= _axes[ai].size()) return -1;
      
      int j = _index(ai+1, is...);
      if (j < 0) return -1;
	
      return _axes[ai].size() * j + i;
    }
    /** 
	Get index correspnding to axes indexes. 

	@param ai   Axes number 
	@param i    First axis index 
	
	@return Global index 
    */
    template <typename I>
    int _index(size_t ai, const I& i) const { return i;  }
    /** 
	@{ 
	@name Bin index 
    */
    /** 
	Find bin index of values (recursive template expansion) 

	@param ai   Axes index 
	@param x    Value 
	@param xs   Remaning values 

	@return bin index or -1 if out of bounds 
    */
    template <typename X1, typename ... Xs>
    int _find_bin(size_t ai, const X1& x, const Xs& ... xs) const {
      int j = _find_bin(ai+1,xs...);
      int k = _axes[ai].find_bin(x);
      
      if (j < 0 or k < 0) return -1;

      return _axes[ai].size() * j + k;
    }
    /** 
	Find bin index of values (terminal of recursive template
	expansion)

	@param ai   Axes index 
	@param x    Value 

	@return bin index or -1 if out of bounds 
    */
    template <typename X1>
    size_t _find_bin(size_t ai, const X1& x) const {
      return _axes[ai].find_bin(x);
    }
    /** @} */

    /**
       @{
       @name Promote weights and bin widths to compuation type
    */
    dw_type _w_as_dw() const
    {
      dw_type r(_w.size());
      std::copy(std::begin(_w),std::end(_w),std::begin(r));
      return r;
    }
    dw_type _dx_as_dw() const
    {
      dx_type d(dx());
      dw_type r(d.size());
      std::copy(std::begin(d),std::end(d),std::begin(r));
      return r;
    }
    /** @} */
    
    /** 
	@{ 
	@name Data members 
    */
    /** Sum of weights in each bin */
    w_type _w;

    /** References to axes objects used */
    axes_type _axes;
    /** @} */
    /** 
	IO structure `io` is a friend 
    */
    template <template <typename> class Trait>
    friend struct io;
  };

  /** 
      Add to histograms together and return the sum 
      
      @param lhs Left-hand side 
      @param rhs Right-hand side 

      @return Sum of the two histograms 

      @throws std::logic_error in case the histograms are not compatible 

      @ingroup nd_histogram 
  */
  template <size_t N, typename W, typename X>
  histogram<N,W,X> operator+(const histogram<N,W,X>& lhs,
			     const histogram<N,W,X>& rhs)
  {
    histogram<N,W,X> ret(lhs.axes());
    ret += lhs;
    ret += rhs;

    return ret;
  }
  /** 
      Compare two histograms for equality.  Histograms are considered
      equal if and only if

      - The have the same bin boundaries 
      - The have the same sum of weights 
      
      @param lhs Left-hand side 
      @param rhs Right-hand side 

      @return true if equal 
  */
  template <size_t N, typename W, typename X>
  bool operator==(const histogram<N,W,X>& lhs,
		  const histogram<N,W,X>& rhs)
  {
    return lhs.operator==(rhs);
  }
    
  //==================================================================
  /** 
      Extract values from histogram and store as constant values.

      The two argument constructor allows one to retrieve a scaled or
      normalised version of the histogram as constant values.
  */
  template <size_t N, typename W, typename X=double>
  struct frozen_histogram {
    /** Histogram type */
    using histogram_type=histogram<N,W,X>;
    /** Weights type */
    using w_type=typename histogram_type::w_type;
    /** X type */
    using x_type=typename histogram_type::x_type;
    /** DX type */
    using dx_type=typename histogram_type::dx_type;
    /** DX type */
    using dw_type=typename histogram_type::dw_type;
    /** Element type */
    using d_type=typename histogram_type::d_type;
    /** Size type */
    using n_type=std::valarray<size_t>;

    /** Create frozen histogram from histogram */
    frozen_histogram(const histogram_type& h)
      : x(h.x()),
	dwdx(h.dwdx()),
	edwdx(h.edwdx()),
	dx(h.dx()),
	n{_sizes(h.axes())}
    {
    }
    /** Create a frozen histogram from histogram */
    template <typename Operation>
    frozen_histogram(const histogram_type& h, Operation operation)
      : frozen_histogram(h.x(),
			 operation(h),
			 h.dx(),
			 _sizes(h.axes()))
    {
    }

    /** Get index from axes indexes */
    template <typename ...I>
    int index(const I& ...i) const { return _index(0,i...); }

    /** Get the integral */
    d_type integral() const { return (dwdx * dx).sum();  }
    /** 
	@{ 
	@name Data 
    */
    /** X values */
    const x_type x;
    /** dW/dx values */
    const dw_type dwdx;
    /** Uncertainty on dW/dx */
    const dw_type edwdx;
    /** dW/dx values */
    const dx_type dx;
    /** Axes sizes */
    const n_type n;
    /** @} *

    /** 
	@{ 
	@name Aliases 
    */
    /** Alias for dwdx */
    const dw_type& y=dwdx;
    /** Alias for edwdx */
    const dw_type& e=edwdx;
    /** @} */
    
  protected:
    /** Protected constructor */
    frozen_histogram(const x_type& x,
		     const std::tuple<dw_type,dw_type> dwdx,
		     const dx_type& dx,
		     const n_type& n)
      : x(x),
	dwdx(std::get<0>(dwdx)),
	edwdx(std::get<1>(dwdx)),
	dx(dx),
	n(n)
    {
    }
    /** 
	Extract axes sizes 
    */
    n_type _sizes(const typename histogram_type::axes_type& a) const {
      n_type ret(a.size());
      size_t i = 0;
      for (auto& nn : ret) nn = a[i++].size();
      return ret;
    }
    /** 
	Get index correspnding to axes indexes. 

	@param ai   Axes number 
	@param i    First axis index 
	@param is   Remaming axes indexes 
	
	@return Global index 
    */
    template <typename I, typename ...Is>
    int _index(size_t ai, const I& i, const Is& ...is) const {
      if (i < 0 or i >= n[ai]) return -1;

      int j = _index(ai+1, is...);
      if (j < 0) return -1;

      return n[ai] * j + i;
    }
    /** 
	Get index correspnding to axes indexes. 

	@param ai   Axes number 
	@param i    First axis index 
	
	@return Global index 
    */
    template <typename I>
    int _index(size_t ai, const I& i) const { return i; }
  };

  //==================================================================
  /**
     Normalize a histogram to its integral.  This returns a tuple of
     scaled weights and uncertainties.

     Should only be used with frozen_histogram 
  */
  template <size_t N, typename W, typename X>
  std::tuple<typename histogram<N,W,X>::dw_type,
	     typename histogram<N,W,X>::dw_type>
  normalize(const histogram<N,W,X>& hist)
  {
    auto integral = hist.integral();
    return make_tuple(hist.dwdx()  / integral,
		      hist.edwdx() / integral);
  }

  //------------------------------------------------------------------
  /**
     Scale a histogram by a value.  This returns a tuple of scaled
     weights and uncertainties.

     Should only be used with frozen_histogram 
  */
  template <size_t N, typename W, typename X>
  std::tuple<typename histogram<N,W,X>::dw_type,
	     typename histogram<N,W,X>::dw_type>
  scale(const histogram<N,X,W>& hist,
	const typename histogram<N,W,X>::dw_type& scale)
  {
    auto integral = hist.integral();
    return make_tuple(hist.dwdx()  * scale,
		      hist.edwdx() * scale);
  }

  //==================================================================
  /** A binary wrapper of a reference to a value */
  template <typename T>
  struct to_binary {
    to_binary(const T& v) : _v(v) {};
	
    const T& _v;
  };
  /** Write a binary wrapper to output stream */
  template <typename T>
  std::ostream& operator<<(std::ostream& o, const to_binary<T>& b)
  {
    o.write(reinterpret_cast<const char*>(&b._v), sizeof(T));
    return o;
  }
  //------------------------------------------------------------------
  /**
     Holder of read value when reading in binary mode.

     This contains a variant between the type T and a reference to
     such a type.  That means, if we constructor the wrapper from a
     reference, then the variant will be the reference type,
     otherwise, it is a value.  The first case comes about when we do

     @code
     from_binary<T> w(reference);
     @encode

     while the second case is when we do (possibly indirectly)

     @code
     from_binary<T> w;
     @endcode

     The latter case is what is used when we use or example an
     istream_iterator
  */
  template <typename T>
  struct from_binary {
    using value=std::variant<std::reference_wrapper<T>,T>;
    /** Construct as a value */
    from_binary() : _v(T()) {}
    /** Construct as a reference */
    from_binary(T& v) : _v(std::ref(v)) {};
    /** The variant */
    value _v;

    /** How to handle whitespace whwn reading an array */
    static void array_ws(std::istream& in)
    {
      in.unsetf(std::ios_base::skipws);
    }
    /** How to handle whitespace whwn reading a value */
    static void value_ws(std::istream& in)
    {
      // in.unsetf(std::ios_base::skipws);
    }

    operator T() const
    {
      T w = std::get<T>(_v);
      return w;
    }
  };
  /**
     Read from an input stream into a binary wrapper
  */
  template <typename T>
  std::istream& operator>>(std::istream& i, from_binary<T>& b)
  {
    T v;
    i.read(reinterpret_cast<char*>(&v), sizeof(T));
    
    if (std::holds_alternative<std::reference_wrapper<T>>(b._v)) 
      std::get<std::reference_wrapper<T>>(b._v).get() = v;
    else 
      b._v = v;
    
    return i;
  }
  //------------------------------------------------------------------
  /**
     Formatted wrapper.  Contains a reference to the value written
  */
  template <typename T>
  struct to_formatted {
    to_formatted(const T& v) : _v(v) {};
    const T& _v;
  };
  /** Write to an output stream from a formatted wrapper */
  template <typename T>
  std::ostream& operator<<(std::ostream& o, const to_formatted<T>& b)
  {
    return o << b._v;
  }
  //------------------------------------------------------------------
  /**
     Holder of read value when reading in formatted (ASCII) mode.

     This contains a variant between the type T and a reference to
     such a type.  That means, if we constructor the wrapper from a
     reference, then the variant will be the reference type,
     otherwise, it is a value.  The first case comes about when we do

     @code
     from_formatted<T> w(reference);
     @encode

     while the second case is when we do (possibly indirectly)

     @code
     from_formatted<T> w;
     @endcode

     The latter case is what is used when we use or example an
     istream_iterator
  */
  template <typename T>
  struct from_formatted {
    using value=std::variant<std::reference_wrapper<T>,T>;

    /** Construct as a value */
    from_formatted() : _v(T()) {}
    /** Construct as a reference  */
    from_formatted(T& v) : _v(std::ref(v)) {}
    /** The variant */
    value _v;
    /** How to treat white space when reading an array */
    static void array_ws(std::istream& in)
    {
      in.setf(std::ios_base::skipws);
    }
    /** How to treat white space when reading a value */
    static void value_ws(std::istream& in)
    {
      in.unsetf(std::ios_base::skipws);
    }
    /** Get the value (not reference) */
    operator T() const  { return std::get<T>(_v); }
  };
  /**
     Read from an input stream into a formatted wrapper
  */
  template <typename T>
  std::istream& operator>>(std::istream& i, from_formatted<T>& b)
  {
    T v;
    i >> v;
    if (std::holds_alternative<std::reference_wrapper<T>>(b._v))  
      std::get<std::reference_wrapper<T>>(b._v).get() = v;
    else 
      b._v = v;
    
    return i;
  }
  //==================================================================
  template <typename T>
  struct formatted_trait
  {
    using from_type = from_formatted<T>;
    using to_type = to_formatted<T>;
    constexpr static char sep = ' ';
  };
  template <typename T>
  struct binary_trait
  {
    using from_type = from_binary<T>;
    using to_type = to_binary<T>;
    constexpr static char sep = '\0';
  };
  
  //==================================================================
  /**
     Input/output service functions.  This type is templated on the
     formatting trait - either @c formatted_trait or @c binary_trait.
  */
  template <template <typename> class Trait=formatted_trait>
  struct io {
    template <typename T>
    using trait_type = Trait<T>;
    template <typename T>
    using from_type = typename Trait<T>::from_type;
    template <typename T>
    using to_type = typename Trait<T>::to_type;
    
    /** 
	Read in an axis object 

	@param in  Input stream 
	
	@return axis object 
    */
    template <typename T>
    static axis<T> read_axis(std::istream& in) {
      using axis_type = axis<T>;
      using x_array   = typename axis_type::x_array;
      using x_type    = typename axis_type::x_type;
      
      size_t n;
      char sep;
      from_type<size_t> c(n);

      in >> c;
      from_type<x_type>::value_ws(in);
      in >> sep;
      
      from_type<x_type>::array_ws(in);
      x_array a(n);
      std::copy_n(std::istream_iterator<from_type<x_type>>(in),
		  n,std::begin(a));
      
      return axis_type(a);
    }
    /** 
	Write out an axis object 

	@param out  Output stream 
	@param a    Axis object 
    */
    template <typename T>
    static void write_axis(std::ostream& out,
			   const axis<T>& a) {
      using axis_type = axis<T>;
      using x_type    = typename axis_type::x_type;
      char  sep       = out.fill();

      out << to_type<size_t>(a.bounds().size()) << sep;
      std::copy(std::begin(a.bounds()),std::end(a.bounds()),
		std::ostream_iterator<to_type<x_type>>(out,
						       std::string(1,sep)
						       .c_str()));
    }
    /** 
	Read in a histogram 
	
	@param in Input stream 
	
	@return pair of axes and histogram 
    */
    template <size_t N, typename W, typename X=double,
	      typename ... A>
    static histogram<N,W,X>
    read_histogram(std::istream& in, const A& ... axes) {
      using histogram_type = histogram<N,W,X>;

      histogram_type hist(axes...);

      from_type<W>::array_ws(in);
      auto&  w = hist._w;
      size_t n = w.size();
      std::copy_n(std::istream_iterator<from_type<W>>(in),n,std::begin(w));

      return hist;
    }
    /**
       Append a read axis to array of axes
    */
    template <size_t N, typename X,size_t ...I>
    static std::array<const axis<X>,N+1>
    append_axis(std::istream& in,
		std::array<const axis<X>,N>& a,
		std::index_sequence<I...>)
    {
      return {{a[I]...,read_axis<X>(in)}};
    }
    /**
       Recurse to read N axes objects from stream
    */
    template <size_t N, typename X>
    static typename std::enable_if<N!=1,std::array<const axis<X>,N>>::type
    read_axes(std::istream& in)
    {
      auto tmp = read_axes<N-1,X>(in);
      return append_axis<N-1,X>(in,tmp,std::make_index_sequence<N-1>{});
    }
    /**
       Catch for last read of axis from stream
    */
    template <size_t N, typename X>
    static typename std::enable_if<N==1,std::array<const axis<X>,N>>::type
    read_axes(std::istream& in)
    {
      return std::array<const axis<X>,1>{read_axis<X>(in)};
    }
    
    template <size_t N, typename W, typename X=double>
    static histogram<N,W,X>
    read_histogram(std::istream& in) {
      using histogram_type = histogram<N,W,X>;
      using axis_type      = typename histogram_type::axis_type;
      using axes_type      = typename histogram_type::axes_type;

      auto axes = read_axes<N,X>(in);
      histogram_type hist(axes);

      from_type<W>::array_ws(in);
      auto&  w = hist._w;
      size_t n = w.size();
      std::copy_n(std::istream_iterator<from_type<W>>(in),n,std::begin(w));

      return hist;
    }
      
    /** 
	Write histogram to stream 
	
	@param out Output stream
	@param h Histogram to write 
    */
    template <size_t N, typename W, typename X>
    static void write_histogram(std::ostream& out,
				const histogram<N,W,X>& h) {
      auto& axes = h.axes();
      char  sep  = out.fill();
      for (auto& a : axes) write_axis<X>(out, a);
      std::copy(std::begin(h.w()),std::end(h.w()),
		std::ostream_iterator<to_type<W>>(out,
						  std::string(1,sep)
						  .c_str()));
    }
  };
  //------------------------------------------------------------------
  /** I/O specialisation using formatted output */
  using formatted_io=io<formatted_trait>;
  /** I/O specialisation using binary output (not platform indenendent) */
  using binary_io=io<binary_trait>;
  
  //==================================================================
  /**
     Wrap double for fixed precision and width when streaming
  */
  template <size_t width,
	    size_t precision,
	    std::ios_base::fmtflags fmt=std::ios_base::fixed>
  struct wdouble {
    /** 
	Construct from a value 
	
	@param x Value 
    */
    wdouble(double x) : _x(x) {};
    /** The value */
    double _x;
  };
  //------------------------------------------------------------------
  /**
     Stream out wrapped double
     
     @param o  Output stream 
     @param x  Value 
     
     @return @a o 
  */
  template <size_t width,
	    size_t precision,
	    std::ios_base::fmtflags fmt>
  std::ostream& operator<<(std::ostream& o,
			   const wdouble<width,precision,fmt>& x) {
    auto op = o.precision(precision);
    auto of = o.setf(fmt,std::ios_base::floatfield);
    o << std::setw(width) << x._x;
    o.precision(op);
    o.setf(of,std::ios_base::floatfield);
    return o;
  }
  
  //==================================================================
  /** 
      Base class template of ASCII plotter 
  */
  template <size_t N, typename W, typename X>
  struct ascii_plotter;

  //------------------------------------------------------------------
  /** 
      Class template specialisation of ASCII plotter for 1D histograms 
  */
  template <typename W,typename X>
  struct ascii_plotter<1,W,X>
  {
    using frozen=frozen_histogram<1,W,X>;
    using bare=histogram<1,W,X>;
    
    /** 
	Plot a histogram as ASCII art. 
	
	@tparam XWidth Width of independent variable values 
	@tparam XPrec  Precision of independent variable values 
	@tparam XFmt   Format of independent variable values 
	@tparam YWidth Width of dependent variable values 
	@tparam YPrec  Precision of dependent variable values 
	@tparam YFmt   Format of dependent variable values 

	@param o     Output stream 
	@param f     Frozen histogram to plot 
	@param width Maximum width of histogram bars 
    */
    template <size_t XWidth=4, size_t XPrec=1, 
	      std::ios_base::fmtflags XFmt=std::ios_base::fixed,
	      size_t YWidth=8, size_t YPrec=3, 
	      std::ios_base::fmtflags YFmt=std::ios_base::fixed>
    static void plot(std::ostream& o,
		     const frozen& f,
		     size_t width=80)
    {
      size_t w = width - 3 - 1 - XWidth - 2 - YWidth - 5 - YWidth;
      auto mx    = (f.dwdx+f.edwdx).max();
      auto fact  = mx == W{} or std::isnan(mx) or std::isinf(mx) ? 0 : w/mx;
      for (size_t i = 0; i < f.x.size(); i++) {
	size_t estar = std::max(size_t(fact*f.edwdx[i]),0ul);
	size_t nstar = size_t(         fact*f.dwdx[i]) - estar;
	o << std::setw(3) << i << " "
	  << wdouble<XWidth,XPrec,XFmt>(f.x[i][0]) << ": "
	  << wdouble<YWidth,YPrec,YFmt>(f.dwdx[i]) << " +/- "
	  << wdouble<YWidth,YPrec,YFmt>(f.edwdx[i]) << " "
	  << std::string(nstar,'*')
	  << std::string(estar,'-')
	  << std::endl;
      }
    }
    /** 
	Plot a histogram as ASCII art. 
	
	@tparam XWidth Width of independent variable values 
	@tparam XPrec  Precision of independent variable values 
	@tparam XFmt   Format of independent variable values 
	@tparam YWidth Width of dependent variable values 
	@tparam YPrec  Precision of dependent variable values 
	@tparam YFmt   Format of dependent variable values 

	@param o     Output stream 
	@param f     Histogram to plot 
	@param width Maximum width of histogram bars 
    */
    template <size_t XWidth=4, size_t XPrec=1, 
	      std::ios_base::fmtflags XFmt=std::ios_base::fixed,
	      size_t YWidth=8, size_t YPrec=3, 
	      std::ios_base::fmtflags YFmt=std::ios_base::fixed>
    static void plot(std::ostream& o,
		     const bare& h,
		     size_t width=80)
    {
      frozen f(h);
      plot<XWidth,XPrec,XFmt,YWidth,YPrec,YFmt>(o, f, width);
    }
  };
  template <size_t N, typename W, typename X>
  ascii_plotter<N,W,X> make_plotter(const frozen_histogram<N,W,X>& h)
  {
    return ascii_plotter<N,W,X>();
  }
  template <size_t N, typename W, typename X>
  ascii_plotter<N,W,X> make_plotter(const histogram<N,W,X>& h)
  {
    return ascii_plotter<N,W,X>();
  }
  
  //==================================================================
  /** 
      @example example1d.cc  Example of one-dimensional histogram 

      This example shows the basic API, and the one-dimensional
      specialisation.

      We generate samples of a random variable 

      @f[ X\sim\mathcal{N}[0,1]\quad,@f] 

      and weights  

      @f[ W\sim\mathcal{U}[0,1]\quad,@f] 

      and histogram the sample with the weights given.  
  */
  //------------------------------------------------------------------
  /** 
      @example example1di.cc  Example of one-dimensional histogram 

      Same as example1d.cc but using integer weights.   
  */
  //------------------------------------------------------------------
  /** 
      @example example2d.cc  Example of two-dimensional histogram 

      This example shows the basic API.

      We generate samples of a random variables 

      @f{align*}{
      X&\sim\mathcal{N}[0,1]\cr
      Y&\sim\mathcal{E}[1]\quad,
      @f} 

      and histogram the sample.  We print out the data of the histogram. 
  */
  //------------------------------------------------------------------
  /** 
      @test Test of 2D histogram 
      @example test2d.cc Test of 2D histogram 

      Tests calculation of bin widths, centres, indexes, and so on.
  */
  //------------------------------------------------------------------
  /** 
      @test Test of 3D histogram 
      @example test3d.cc Test of 3D histogram 

      Tests calculation of bin widths, centres, indexes, and so on.
  */
  //------------------------------------------------------------------
  /** 
      @test Test of adding histogram 
      @example testadd.cc Test of adding histogram 

      Tests addition of histograms. 
  */
  //------------------------------------------------------------------
  /** 
      @test Test of I/O
      @example testio.cc Test of I/O

      Tests of I/O
  */
}
#endif
// Local Variables:
//   compile-command: "g++ -c -std=c++11 histogram.hh"
// End:
